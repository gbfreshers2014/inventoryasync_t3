package com.gwynniebee.inventoryAsync.constants;

public class JobConstants {
	public static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";

	public static final String DATA_SOURCE_NAME = "jdbc:mysql://induction-dev-t3.gwynniebee.com/t3";

	public static final String DATABASE_USERNAME = "write_all_bi";

	public static final String DATABASE_PASSWORD = "write_all_bi";
	
	public static final String ADMIN_ID="nchandana@gwynniebee.com";
	
	//public static final String DATA_SOURCE_NAME = "jdbc:mysql://localhost/t3";

	//public static final String DATABASE_USERNAME = "root";

	//public static final String DATABASE_PASSWORD = "password";
}
