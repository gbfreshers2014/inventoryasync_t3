package com.gwynniebee.inventoryAsync.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.async.service.api.JobDefinition;
import com.gwynniebee.async.service.api.JobObjectMapper;
import com.gwynniebee.async.service.api.JobOperationStatus;
import com.gwynniebee.async.service.api.JobStatus;
import com.gwynniebee.async.service.api.JobTask;
import com.gwynniebee.async.service.client.AsyncServiceRestletClient;


public final class SendEmail {
	
	
	private SendEmail() {
		
	}
	
	 private static final Logger LOG = LoggerFactory.getLogger(SendEmail.class);
	 
	 /**
	     * send e-mail notifications.
	     * @param emails String
	     * @param subject subject of the email
	     * @param message email message
	     * @return boolean rv
	     */
	    public static boolean sendEmailNotification(String emails, String subject, String message) {
	        boolean rv = false;

	        String jobName = "InventoryMail" + "-Notify-" + UUID.randomUUID().toString();
	        String jobType = "SendMailJob";

	        AsyncServiceRestletClient client = null;
	        try {
	            Map<String, String> jobMap = null;
	            jobMap = getEmailJobMap(emails, subject, message);

	            JobDefinition jobDef = new JobDefinition();
	            jobDef.setJobName(jobName);
	            jobDef.setParamMap(jobMap);

	            JobTask jobTask = new JobTask();
	            jobTask.setJobDefinition(jobDef);
	            jobTask.setJobExecutionUrl(null);
	            jobTask.setNotificationMap(null);
	            jobTask.setSchedule(null);

	            // instantiate the client
	    //        ServiceProperties serviceProps = ServiceProperties.getInstance();
	            // String asyncServiceUrl =
	            // serviceProps.getProperty(ServiceProperties.ASYNC_SERVICE_URL);
	            String asyncServiceUrl = "http://api-dev.gwynniebee.com:4080/v1/async-job-service";
	            LOG.info("Schduling to server ... " + asyncServiceUrl);
	            client = new AsyncServiceRestletClient(asyncServiceUrl);
	            LOG.info("To schedule: " + JobObjectMapper.INSTANCE.writeValueAsString(jobTask));
	            JobOperationStatus status = client.scheduleJob(jobType, jobTask);
	            LOG.info("Scheduled: " + JobObjectMapper.INSTANCE.writeValueAsString(status));
	            if (status.getCode() != 0) {
	                LOG.error("ASYNC Error: " + status.getMessage());
	                return rv;
	            }

	            // now check
	            JobStatus jobStatus = client.getJobStatus(jobType, jobName);
	            LOG.info("jobStatus: " + JobObjectMapper.INSTANCE.writeValueAsString(jobStatus));
	            if (jobStatus.getCode() != 0) {
	                LOG.error("ASYNC Error: " + jobStatus.getMessage());
	                return rv;
	            }

	            rv = true;
	        } catch (Exception exception) {
	            StringBuffer sbMessage = new StringBuffer();
	            sbMessage.append("Unable to send notification. JobName: ");
	            sbMessage.append(jobName);
	            sbMessage.append(", JobGroup: ");
	            sbMessage.append(jobType);
	            sbMessage.append(", Error: ");
	            sbMessage.append(exception.getMessage());
	            LOG.error(sbMessage.toString(), exception);
	        }

	        return rv;
	    }
	    
	    
	    /**
	     * Get the jobMap for e-mail notification.
	     * @param emails String
	     * @param subject subject of the email
	     * @param message email message
	     * @return jobMap Map<String, String>
	     */
	    
	    private static Map<String, String> getEmailJobMap(String emails, String subject, String message) {

	        Map<String, String> jobMap = new HashMap<String, String>();

	        String smtpHost = "localhost";

	    //    ServiceProperties serviceProps = ServiceProperties.getInstance();
	        // String senderEmailId =
	        // serviceProps.getProperty(ServiceProperties.OUTBOUND_JOB_SENDER);
	        String senderEmailId = "hive@gwynniebee.com";
	        jobMap.put("recipient", emails);
	        jobMap.put("subject", subject);
	        jobMap.put("message", message);
	        jobMap.put("smtp_host", smtpHost);
	        jobMap.put("sender", senderEmailId);

	        return jobMap;
	    }


}

