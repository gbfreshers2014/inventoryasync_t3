package com.gwynniebee.inventoryAsync.response;

import com.gwynniebee.inventoryAsync.tables.DueInventory;
import com.gwynniebee.rest.common.response.AbstractResponse;
import com.gwynniebee.rest.common.response.ResponseStatus;

public class DueInventoryResponse extends AbstractResponse {
	
	private DueInventory inventory;
    private ResponseStatus status;
    
    public DueInventoryResponse(DueInventory inventory, ResponseStatus status) {
        this.inventory = inventory;
        this.status = status;
    }
    
    public DueInventoryResponse() {
        // TODO Auto-generated constructor stub
    }
    
    public DueInventory getInventory() {
        return inventory;
    }
    
    public void setInventory(DueInventory inventory) {
        this.inventory = inventory;
    }
    
    public ResponseStatus getStatus() {
        return status;
    }
    
    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

} 

