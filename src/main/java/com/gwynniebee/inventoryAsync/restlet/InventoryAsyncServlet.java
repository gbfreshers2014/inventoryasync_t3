/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryAsync.restlet;

import javax.servlet.ServletException;

import com.gwynniebee.async.job.helper.AsyncJobRestletServlet;
import com.gwynniebee.inventoryAsync.resources.InventoryAsyncResource;

/**
 * Barcode file upload service routing and controlling resources.
 * @author Sarath
 */

public class InventoryAsyncServlet extends AsyncJobRestletServlet {
    private static final int NUM_THREADS = 3;
    public InventoryAsyncServlet() {
        super(NUM_THREADS, "", InventoryAsyncResource.class);
    }
    @Override
    public void init() throws ServletException {
        try {
           // SearchDataLoaderJobConfig.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.init();
    }
    @Override
    public void destroy() {
        super.destroy();
    }

}
