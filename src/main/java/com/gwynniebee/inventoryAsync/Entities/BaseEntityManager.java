package com.gwynniebee.inventoryAsync.Entities;

import org.skife.jdbi.v2.DBI;

public class BaseEntityManager {
    
    /**
     * Default constructor.
     */
    protected BaseEntityManager() {
    }

    private static DBI dbi;

    /**
     * @param dbi the dbi to set
     */
    public static void setDbi(DBI dbi) {
        BaseEntityManager.dbi = dbi;
    }

    /**
     * @return the dbi
     */
    public static DBI getDbi() {
        return dbi;
    }

}
