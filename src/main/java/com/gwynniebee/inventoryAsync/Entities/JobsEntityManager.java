package com.gwynniebee.inventoryAsync.Entities;

import java.util.List;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryAsync.constants.JobConstants;
import com.gwynniebee.inventoryAsync.dao.InventoryAsyncDAO;
//import com.gwynniebee.inventoryAsync.dao.ProductNameao;
import com.gwynniebee.inventoryAsync.tables.DueInventory;


public class JobsEntityManager extends BaseEntityManager {


	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory
			.getLogger(JobsEntityManager.class);
	private static final JobsEntityManager MANAGER_INSTANCE = new JobsEntityManager();

	/**
	 * Get all carrier.
	 * 
	 * @return list of all {@link Carrier} objects
	 * @throws ClassNotFoundException
	 */

	public List<DueInventory> dueUserListcall() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		DBI dbi = new DBI(JobConstants.DATA_SOURCE_NAME,
				JobConstants.DATABASE_USERNAME,
				JobConstants.DATABASE_PASSWORD);
		List<DueInventory> dueList = null;
		InventoryAsyncDAO dao = null;
		Handle h = null;
		h = dbi.open();
		h.begin();
		dao = h.attach(InventoryAsyncDAO.class);
		try {

			dueList = dao.getDueInventoryList();

		} finally {
			if (dao != null) {
				h.commit();
				dao.close();

			}

		}

		return dueList;
	}
	public static JobsEntityManager getInstance() {
		return MANAGER_INSTANCE;
	}
}
