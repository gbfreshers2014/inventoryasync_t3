package com.gwynniebee.inventoryAsync.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.inventoryAsync.tables.DueInventory;


public class InventoryAsyncMapper implements ResultSetMapper<DueInventory>{
	@Override
	public DueInventory map(int index, ResultSet r, StatementContext ctx)
			throws SQLException {

		DueInventory inventory = new DueInventory();

		inventory.setItemId(r.getInt("itemId"));
		inventory.setEmployeeId(r.getString("employeeId"));
		inventory.setExpectedReturn(r.getString("expectedReturn"));

		return inventory;
	}

}
