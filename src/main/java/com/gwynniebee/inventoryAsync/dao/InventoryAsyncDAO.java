package com.gwynniebee.inventoryAsync.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.inventoryAsync.dao.mapper.InventoryAsyncMapper;
import com.gwynniebee.inventoryAsync.tables.DueInventory;


@RegisterMapper(InventoryAsyncMapper.class)
public interface InventoryAsyncDAO extends Transactional<InventoryAsyncDAO>{
	@SqlQuery("select employeeId,expectedReturn,itemId from TRANSACTION where expectedReturn<curdate() and status=1")
	List<DueInventory> getDueInventoryList();

	/**
	 * closes the underlying connection.
	 */
	void close();
}