/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryAsync.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.node.ObjectNode;
import org.joda.time.DateTime;
import org.restlet.data.Status;
import org.restlet.resource.ClientResource;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.async.job.api.AsyncJobRequestMap;
import com.gwynniebee.async.job.api.AsyncJobStatus;
import com.gwynniebee.async.job.helper.AsyncJobResourceBase;
import com.gwynniebee.async.utils.JodaDateTimeSerializer;
import com.gwynniebee.inventoryAsync.Entities.JobsEntityManager;
import com.gwynniebee.inventoryAsync.constants.JobConstants;
import com.gwynniebee.inventoryAsync.tables.DueInventory;
import com.gwynniebee.inventoryAsync.utils.SendEmail;
//import com.gwynniebee.inventoryJob.constants.JobsConstants;
//import com.gwynniebee.inventoryJob.util.EmailClass;
import com.gwynniebee.rest.common.response.ResponseStatus;

/**
 * emp_job class. To change this resource rename the class to the
 * required class.
 * @author Chandana
 */
public class InventoryAsyncResource extends AsyncJobResourceBase {

    private static final Logger LOG = LoggerFactory.getLogger(InventoryAsyncResource.class);    
    public InventoryAsyncResource()
    {
        super(AsyncJobRequestMap.class);
    }
    @Override
    public String getJobName()
    {
        return "inv-async-job";
    }
    @Override
    public String getJobType() {
        return "inv-async-job-type";
    }

    /* (non-Javadoc)
     * @see com.gwynniebee.async.job.helper.AsyncJobResourceBase#estimateCompletionSeconds()
     */
    @Override
    protected int estimateCompletionSeconds() {
        return 100;
    }
    
    
    
    @Override
    protected AsyncJobStatus<?> executeBody() throws Exception {

        final AsyncJobRequestMap request = (AsyncJobRequestMap) this.getJobRequest();
        final LinkedHashMap<String, Object> jobData = request.getJobData();
        final Map<String, Object> result = new HashMap<String, Object>(jobData);
        Date start = new Date();
        result.put("message", result.get("name")
                + ": Greetings from " + getJobType() + "." + getJobName()
                + " on " + JodaDateTimeSerializer.FORMATTER.print(new DateTime()));
        
        
        //LOG.debug("EXECUTEEEEEEEEEEEEEEEEEEEEEEEEEEED");
        LOG.info("STARTED####################################################################################################");
        List<DueInventory> dueList = JobsEntityManager.getInstance().dueUserListcall();
        //List<DueInventory> dueList;
		List<String> ListForAdmin = new ArrayList<String>();
		ListForAdmin.add("USER-ID" + "PRODUCT"+ "EXPECTED RETURN DATE ");
		
        
       
		for (int i = 0; i < dueList.size(); i++) {
			
			DueInventory inventory = dueList.get(i);

			ListForAdmin.add(inventory.getEmployeeId() + "			" + inventory.getItemId() + "			 "
					+ inventory.getExpectedReturn());
			LOG.info("*************************************************************************************************");
			LOG.info("Inventory Details----------" + inventory.getEmployeeId() + "			" + inventory.getItemId() + "			 "
					+ inventory.getExpectedReturn());
			mailEmployee(inventory.getItemId(), inventory);
		}
		
		mailAdmin(ListForAdmin);

		LOG.info("my code executed////////////////////////////////////////////////////////////////////////////////////");

		Date end = new Date();
		String subject = getJobType() + "." + getJobName() + " has completed";
		String body = getJobType() + "." + getJobName() + " completed in "
				+ Long.toString(end.getTime() - start.getTime())
				+ " milliseconds";
		LOG.info("****************************************************************************************         body:"+ body);

		return new AsyncJobStatus<Map<String, Object>>(result, subject, body);
	}
		
        
	private void mailAdmin(List<String> listForAdmin) {
		String to = JobConstants.ADMIN_ID;
		//String from = "hive@gwynniebee.com";
		String subject = "list of due inventory";

		SendEmail.sendEmailNotification(to, subject, listForAdmin.toString()); 
	
	}

	private void mailEmployee(int Item_Id, DueInventory inventory) {

		String to, subject;
		String bodylines = "";
		//SendEmail.sendEmailNotification("nchandana@gwynniebee.com", "Retrun the "+inventory.getItemId(), "please return ");
		to = inventory.getEmployeeId();
		subject = Item_Id + " expected back from you";

		bodylines += " Hi,";
		bodylines += " The product that you have issued from inventory management"
						+ " is expected back from you on "
						+ inventory.getExpectedReturn() + ".";
		bodylines += "Please return the item with " + Item_Id
				+ " as soon as possible.";
		bodylines +="thanks" ;

						
	/*		for (int i = 0; i < bodylines.size(); i++) {

				LOG.info(bodylines.get(i));
			}
		*/

		//from = "hive@gwynniebee.com";

		SendEmail.sendEmailNotification(to, subject, bodylines);//enable this after bambooing

	}
	
}
