#!/bin/sh
clear
new_project_name='InventoryAsyncJob'
new_properties_name='InventoryAsyncJob_prop'
local_tomcat_path='/var/lib/tomcat7'
package_name='emp_job_api'
resource_name='emp_job'
application_name='InventoryAsyncJob_app'
project_description='project-description'

echo "running templates replace on REST-template Project"
rm -rf .git/
echo "replacing project name \"InventoryAsyncJob\" with \"$new_project_name\""
#grep -rl 'InventoryAsyncJob' .
grep -rl 'InventoryAsyncJob' . | xargs sed -i 's|InventoryAsyncJob|'"$new_project_name"'|g'

echo "replacing \"InventoryAsyncJob_prop.properties\" with \"$new_properties_name.properties\""
#grep -rl 'InventoryAsyncJob_prop.properties' .
grep -rl 'InventoryAsyncJob_prop.properties' . | xargs sed -i 's|InventoryAsyncJob_prop.properties|'"$new_properties_name.properties"'|g'
find ./configuration -name 'InventoryAsyncJob_prop.properties' -exec rename 'InventoryAsyncJob_prop.properties' $new_properties_name.properties '{}' \;

echo "replacing \"/var/lib/tomcat7\" with $local_tomcat_path"
#grep -rl '/var/lib/tomcat7'
grep -rl '/var/lib/tomcat7' . | xargs sed -i 's|/var/lib/tomcat7|'"$local_tomcat_path"'|g'

echo "replacing the description from \"{project-description}\" to \"$project_description\" in pom.xml"
sed -i 's|{project-description}|'"$project_description"'|g' pom.xml

echo "rearranging the files"
mv -v 'src/main/java/com/gwynniebee/{package-name}/restlet/resources/{resource-name}.java' 'src/main/java/com/gwynniebee/{package-name}/restlet/resources/'"$resource_name"'.java'
mv -v 'src/main/java/com/gwynniebee/{package-name}/restlet/{application-name}.java' 'src/main/java/com/gwynniebee/{package-name}/restlet/'"$application_name"'.java'
mv -v 'src/main/java/com/gwynniebee/{package-name}/' src/main/java/com/gwynniebee/$package_name/
mv -v 'src/test/java/com/gwynniebee/{package-name}/' src/test/java/com/gwynniebee/$package_name/

echo "changing the package name from \"{package-name}\" to \"$package_name\""
grep -rl '{package-name}' ./src
grep -rl '{package-name}' ./src | xargs sed -i 's|{package-name}|'"$package_name"'|g'

echo "replacing the resource name from \"RestTemplateResource\" to \"$resource_name\""
grep -rl '{resource-name}' ./src
grep -rl '{resource-name}' ./src | xargs sed -i 's|{resource-name}|'"$resource_name"'|g'

echo "replacing the Application name from \"{GBRestTemplateApplication}\" to \"$application_name\""
grep -rl '{application-name}' ./src
grep -rl '{application-name}' ./src | xargs sed -i 's|{application-name}|'"$application_name"'|g'

#rm template.sh~ template.sh
#end of shell script
#exit 0
